## Application Ionic - Liste des clubs de Ligue 1 - Saison 2018/2019

Cette application a été conçue par Florian MALEVRE, étudiant en M2 MIAGE FIC au sein de l'Université Paris Descartes.
Au sein de celle-ci, vous pourrez retrouver l'ensemble des clubs de football de Ligue 1 pour la saison 2018/2019.

## Données & Plugin

Comme dit précédemment les données utilisées sont propres à des clubs de football. Accessible au chemin suivant : src/assets/api/clubs.json, on y retouve les informations suivantes : le classement de l'équipe, son nom, son écusson, sa date de création ainsi qu'une description sur l'histoire du club.

Pour réaliser cette application, un seul plugin a été nécessaire : Storage. Il permet de faciliter le stockage des paires clé/valeur ainsi que des objets JSON. Il utilise une grande variété de moteurs de stockage et parmi elles, SQLite est celle choisie.
L'installation de ce plugin a pu être réalisé à partir de la commande suivante :

```bash
$ ionic cordova plugin add cordova-sqlite-storage
```
Dans le fichier app.module.ts (disponible en suivant le chemin src/app), il a également fallu ajouter IonicStorageModule.forRoot()
aux différents imports.

## Démarrer l'application

Il faut tout d'abord s'assurer que npm est bien présent sur votre machine. Si cela n'est pas le cas, exécuter la commande suivante dans un terminal :

```bash
$ npm install
```
Il vous suffit ensuite de lancer la commande suivante pour accéder à l'application via un navigateur web :

```bash
$ ionic serve --lab
```
Un  onglet va alors s'ouvrir tout seul et vous pourrez visualiser l'application avant de l'installer sur votre mobile Android. Pour maintenant exécuter l'application sur votre appareil, plusieurs étapes sont à effectuer.

La première est d'ajouter Android comme plateforme :

```bash
$ ionic cordova platform add android
```
La seconde consiste à lancer le build :

```bash
$ ionic cordova build android
```
Puis pour finir, vous pouvez lancer l'application avec :

```bash
$ ionic cordova run android
```
## Descriptif de l'application

Cette application comporte 3 pages (dont le code est disponible au chemin src/pages) : une dédiée aux clubs favoris, une autre regroupant l'intégralité des clubs de Ligue 1 et une dernière permettant d'obtenir des informations propres à un club. Commençons par voir le fonctionnement de la page "Mes équipes favorites". A votre première arrivée , cette dernière est vide et ne dispose que d'un bouton nommé "Rechercher une équipe" (comme le montre la capture nommée "page_accueil_vide.png"). En cliquant sur celui-ci, vous êtes redirigé vers la page "Ligue 1". L'utilisateur de l'application peut y voir l'ensemble des 20 logos des clubs étant présents en Ligue 1 pour la saison 2018/2019. Vous pouvez visualiser au préalable ce rendu au travers des captures "liste_clubs(1).png" et "liste_clubs(2).png". En cliquant sur un club au choix, par exemple le Paris-Saint-Germain, vous êtes redirigé vers la 3ème et dernière page de l'application. En fonction du club choisi, l'utilisateur peut visualiser : le nom du club, son logo, son classement ainsi qu'une description historique du club. L'exemple avec le PSG est disponible avec la capture "info_club.png". Toujours au sein de cette même page, l'utilisateur peut ajouter son club à la liste de ses clubs préférés. Il lui suffit pour cela de cliquer sur l'étoile situer en haut à droite de la page (voir capture "fav_club.png"). Quel impact a cette action sur l'application ? Pour le savoir retournons sur la page "Mes équipes favorites". Comme nous pouvons le voir sur la capture "page_accueil_favoris.png", le club sélectionné apparaît et la page possède un nouvel affichage. Un résumé du club comprenant : son nom, sa date de création et son classement et alors disponible. Par ailleurs, en cliquant sur le club, nous retournous sur la page détaillant ce dernier. Enfin pour terminer, un menu est disponible (voir la capture "menu_appli.png") est permet à l'utilisateur de parcourir les différentes pages contenant ses équipes préférées et l'ensemble des clubs du championnat.

## Sources & Remerciements

Afin de réaliser ce projet, je tenais à remercier M.Sghir pour son introduction au framework Ionic ainsi que le site [SOAT](https://soat.developpez.com/) dont le tutoriel m'a permis de réaliser cette application.



