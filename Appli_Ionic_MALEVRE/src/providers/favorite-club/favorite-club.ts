import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { IClub } from "../../interface/IClub";

const CLUB_KEY = "club_";

@Injectable()
export class FavoriteClubProvider {
  constructor(private storage: Storage) {
    console.log("Hello UserPreferencesProvider Provider");
  }

  addFavoriteClub(club: IClub) {
    this.storage.set(this.getClubKey(club), JSON.stringify(club));
  }

  removeFavoriteClub(club: IClub) {
    this.storage.remove(this.getClubKey(club));
  }

  isFavortieClub(club: IClub) {
    return this.storage.get(this.getClubKey(club));
  }

  toogleFavoriteClub(club: IClub) {
    this.isFavortieClub(club).then(
      isFavorite =>
        isFavorite
          ? this.removeFavoriteClub(club)
          : this.addFavoriteClub(club)
    );
  }

  getClubKey(club: IClub) {
    return CLUB_KEY + club.id.toString();
  }

  getFavoriteClubs(): Promise<IClub[]> {
    return new Promise(resolve => {
      let results: IClub[] = [];
      this.storage
        .keys()
        .then(keys =>
          keys
            .filter(key => key.includes(CLUB_KEY))
            .forEach(key =>
              this.storage.get(key).then(data => results.push(JSON.parse(data)))
            )
        );
      return resolve(results);
    });
  }
}
