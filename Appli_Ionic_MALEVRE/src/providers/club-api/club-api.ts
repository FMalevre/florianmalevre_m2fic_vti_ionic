import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Platform } from "ionic-angular";
import { Observable } from "rxjs/Rx";
import { IClub } from "../../interface/IClub";

@Injectable()
export class ClubApiProvider {
  private baseUrl: string = "../../assets/api/clubs.json";

  clubs: IClub[];

  constructor(
    private readonly http: HttpClient,
    private readonly platform: Platform
  ) {
    console.log("Hello ClubApiProvider Provider");
    if (this.platform.is("cordova") && this.platform.is("android")) {
      this.baseUrl = "/android_asset/www/assets/api/clubs.json";
    }
  }

  getClubs(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
