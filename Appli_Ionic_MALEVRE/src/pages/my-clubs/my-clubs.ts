import { FavoriteClubProvider } from "../../providers/favorite-club/favorite-club";
import { ClubDetailPage } from "../club-detail/club-detail";
import { IClub } from "../../interface/IClub";
import { ClubListPage } from "../club-list/club-list";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-my-clubs",
  templateUrl: "my-clubs.html"
})
export class MyClubsPage {
  favoriteClubs: IClub[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private favoriteClubProvider: FavoriteClubProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyClubsPage");
  }

  ionViewWillEnter() {
    this.initFavoriteClubs();
  }

  initFavoriteClubs() {
    this.favoriteClubProvider
      .getFavoriteClubs()
      .then(favs => (this.favoriteClubs = favs));
  }

  findClub() {
    this.navCtrl.push(ClubListPage);
  }

  goToDetail(club: IClub) {
    this.navCtrl.push(ClubDetailPage, club);
  }
}
