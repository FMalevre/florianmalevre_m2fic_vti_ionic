import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyClubsPage } from './my-clubs';

@NgModule({
  declarations: [
    MyClubsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyClubsPage),
  ],
})
export class MyClubsPageModule {}
