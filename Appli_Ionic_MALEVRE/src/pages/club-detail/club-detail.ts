import { FavoriteClubProvider } from "../../providers/favorite-club/favorite-club";
import { IClub } from "../../interface/IClub";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-club-detail",
  templateUrl: "club-detail.html"
})
export class ClubDetailPage {
  club: IClub;
  isFavorite: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private favoriteClubProvider: FavoriteClubProvider
  ) {}

  ionViewDidLoad() {
    this.club = this.navParams.data;
    this.favoriteClubProvider
      .isFavortieClub(this.club)
      .then(value => (this.isFavorite = value));
  }

  toggleFavorite(): void {
    this.isFavorite = !this.isFavorite;
    this.favoriteClubProvider.toogleFavoriteClub(this.club);
  }
}
