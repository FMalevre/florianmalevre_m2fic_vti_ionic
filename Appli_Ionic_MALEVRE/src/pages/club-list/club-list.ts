import { ClubApiProvider } from "../../providers/club-api/club-api";
import { ClubDetailPage } from "../club-detail/club-detail";
import { IClub } from "../../interface/IClub";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-club-list",
  templateUrl: "club-list.html"
})
export class ClubListPage {
  clubs = new Array<IClub>();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private clubApiProvider: ClubApiProvider
  ) {}

  ionViewDidLoad() {
    this.clubApiProvider.getClubs().subscribe(data => {
      this.clubs = data;
    });
  }

  goToDetail(club: IClub) {
    this.navCtrl.push(ClubDetailPage, club);
  }
}
