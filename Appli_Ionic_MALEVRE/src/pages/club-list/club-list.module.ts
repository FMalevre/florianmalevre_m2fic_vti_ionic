import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ClubListPage } from "./club-list";

@NgModule({
  declarations: [ClubListPage],
  imports: [IonicPageModule.forChild(ClubListPage)]
})
export class ClubListPageModule {}
