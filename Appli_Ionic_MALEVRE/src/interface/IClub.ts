export interface IClub {
  id: number;
  classement: number;
  name: string;
  logo: string;
  description: string;
  date: string;
}
