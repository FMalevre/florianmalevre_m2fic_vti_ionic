import { IonicStorageModule } from "@ionic/storage";
import { HttpClientModule } from "@angular/common/http";
import { ClubDetailPageModule } from "../pages/club-detail/club-detail.module";
import { ClubListPageModule } from "../pages/club-list/club-list.module";
import { MyClubsPageModule } from "../pages/my-clubs/my-clubs.module";
import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";

import { MyApp } from "./app.component";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ClubApiProvider } from "../providers/club-api/club-api";
import { FavoriteClubProvider } from "../providers/favorite-club/favorite-club";

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    MyClubsPageModule,
    ClubListPageModule,
    ClubDetailPageModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ClubApiProvider,
    FavoriteClubProvider
  ]
})
export class AppModule {}
